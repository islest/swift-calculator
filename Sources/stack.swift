struct Stack<E> : Sequence {
  var items : [E]

  init() {
    items = [E]()
  }

  mutating func push(_ item: E) {
    items.append(item)
  }

  mutating func pop() -> E {
    return items.removeLast()
  }

  mutating func flush() {
    items = [E]()
  }

  func top() -> E? {
    return items.last
  }

  func size() -> Int {
    return items.count
  }

  func makeIterator() -> StackIterator<E> {
    return StackIterator<E>(self)
  }
}


struct StackIterator<E> : IteratorProtocol {
  let stack : Stack<E>
  var position: Int  
  init(_ stack : Stack<E>) {
    self.stack = stack
    self.position = 0
  }

  mutating func next() -> E? {
    let nextPosition = self.position + 1
    guard nextPosition < self.stack.size()
      else { return nil }
    
    self.position = nextPosition
    return self.stack.items[self.position]
  }
}







