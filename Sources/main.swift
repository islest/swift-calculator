import Foundation

var testStrings : [(raw: String, result: Int)] = []

testStrings += [(raw: "3 + 4 * 3 + 2", result: 17)]
testStrings += [(raw: "2 * 8 + 100", result: 116)]
testStrings += [(raw: "42 + 2", result: 44)]
testStrings += [(raw: "21 + 3 * 4", result: 33)]
testStrings += [(raw: "42 / 2", result: 21)]
testStrings += [(raw: "42 / 2 / 3", result: 7)]
testStrings += [(raw: "42 / 3 / 2", result: 7)]
testStrings += [(raw: "42 + 6 / 2 + 4", result: 49)]


// Failing tests on simple 2 + 2 equations

for test in testStrings {
    var e : Equation = Equation.init(rawString: test.raw)
    if e.calculate() != test.result {
        print("Test failed on " + test.raw + "!")
        print("Test result was: " + String(e.calculate()))
    } else {
        print("Test passed on " + test.raw + "!")
    }
}
