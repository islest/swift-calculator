// Anything that implements this protocol can call calculate()
protocol Calculable {
    func calculate() -> Int
}

