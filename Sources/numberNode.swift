// Calculable Node that stores a simple number
class NumberNode : Calculable {
    let value : Int
    
    init(num: Int) {
        self.value = num 
    } 

    func calculate() -> Int {
        return self.value 
    }
}

// Generator to generate number node from string "123" for example to
// Node with val == 123
extension NumberNode {
    class func FromString(array: [UnicodeScalar]) -> NumberNode {
        var i : Int = 0, result : Int = 0
        
        for char in array.reversed() {
            if let num : Int = Int(String(char)) {
                result += num * pow(base: 10, power: i)
                
            }
            i += 1
        }        
        return NumberNode.init(num: result)
    }
}
