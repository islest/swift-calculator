func pow(base: Int, power: Int) -> Int {
    if (power == 0) { 
        return 1 
    }
    var result : Int = 1
    for _ in 1...power {
        result *= base
    }
    return result
}
