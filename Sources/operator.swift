// Operator ENUM + Extension for storing operators in their own type for convenience
enum MathOperator : String {
    case Add = "+"
    case Subtract = "-"
    case Multiply = "*"
    case Divide   = "/" 
}

// Order of operations priority extension
extension MathOperator {
    var priority: Int {
        if self == MathOperator.Multiply || self == MathOperator.Divide {
            return 1 
        } else {
            return 0
        }
    }
}
