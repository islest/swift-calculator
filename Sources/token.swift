import Foundation

struct Token {
  static let DigitSet = CharacterSet.decimalDigits
  static let OperatorSet = "+*-/".unicodeScalars
  var value : UnicodeScalar
}

extension Token {
  func isDigit() -> Bool {
     return Token.DigitSet.contains(self.value) ? true : false
  }
}

extension Token {
  func isOperator() -> Bool {
    return Token.OperatorSet.contains(self.value) ? true : false
  }
}
