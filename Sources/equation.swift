class Equation : Calculable {
    private let rawEquation : String
    private let equationArr : [UnicodeScalar]
    var rootNode : Calculable?
    
    init(rawString: String) {
        self.rawEquation = rawString
        self.equationArr = self.rawEquation.unicodeScalars.filter { $0 != " " }
    }
    
    func calculate() -> Int {
        var numberStack : Stack<NumberNode> = Stack<NumberNode>()
        var operatorStack : Stack<OperatorNode> = Stack<OperatorNode>()
        var currentStack: Stack<UnicodeScalar> = Stack<UnicodeScalar>()
        
        for character in self.equationArr {
            let token : Token = Token.init(value : character)
            
            if token.isDigit() {
                currentStack.push(token.value)
            } else if token.isOperator() {
                numberStack.push(NumberNode.FromString(array: currentStack.items))
                currentStack.flush()
        
                let currentOpNode : OperatorNode = OperatorNode.init(operString: String(token.value))
                for node in operatorStack.reversed() {
                    if currentOpNode.op.priority <= node.op.priority {
                        // Chuck two numbers on this node
                        // or chuck one number depending
                        
                        for _ in 0...1 {
                            if numberStack.size() >= 1 {
                                node.addChild(child: numberStack.pop())
                            }
                        }
                    } else {
                        break
                    }
                }            
                operatorStack.push(currentOpNode)
            }
        }

        numberStack.push(NumberNode.FromString(array:currentStack.items))
        currentStack.flush()
            
        for node in operatorStack.reversed() {
            for _ in 0...1 {
                if numberStack.size() >= 1 {
                    node.addChild(child: numberStack.pop())
                }
            }
        }
        
        
        var rootNode : OperatorNode = operatorStack.items.removeFirst()
        for node in operatorStack {
            if node.op.priority > rootNode.op.priority {
                rootNode.addChild(child: node)
            } else {
                let tmp : OperatorNode = rootNode
                rootNode = node
                rootNode.addChild(child: tmp)
            }
        }
        
        return rootNode.calculate() 
    }
}
