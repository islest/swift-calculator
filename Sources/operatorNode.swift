// Stores an operator and its children
class OperatorNode : Calculable {
    var op : MathOperator
    var children : [Calculable]
    
    init(operString: String) {
        if let operand : MathOperator = MathOperator(rawValue: String(operString)) {
            self.op = operand 
        } else { 
            self.op = MathOperator.Add
        }
        self.children = [Calculable]()
    }
    
    func addChild(child: Calculable) {
        self.children.append(child)
    }
    
    func calculate() -> Int {
        var result : Int = 0
        
        switch self.op {
            case MathOperator.Add:
                result = self.children.reduce(0, { $0 + $1.calculate() })
            case MathOperator.Subtract:
                result = self.children.reduce(0, { $0 - $1.calculate() })
            case MathOperator.Multiply:
                result = self.children.reduce(1, { $0 * $1.calculate() })
            case MathOperator.Divide:
                result = self.children.removeLast().calculate()
                
                for child in self.children {
                    result /= child.calculate()
                }
        }
        return result   
    }
}


